export const environment = {
  production: true,
  auth0: {
    clientID: 'h2vM1Gqa2D1DKWhZkONhJmLQwK256uCX',
    domain: 'twormix.eu.auth0.com',
    audience: 'https://sourceglass.twormix.com/',
    redirectUri: 'https://sourceglass.twormix.com/auth_callback',
  }
};
