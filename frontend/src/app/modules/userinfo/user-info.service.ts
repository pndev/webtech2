import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/mergeMap';

import { HttpClient } from '../../http-client.service';
import { AuthService } from '../auth/auth.service'

export class UserInfo {
  public id: string;
  public name: string;
  public address: string;
}

@Injectable()
export class UserInfoService {

    constructor(private authService: AuthService) { }

    private userInfo: UserInfo;

    public getSimpleUserInfo(): Observable<UserInfo> {
        if (this.userInfo) {
            return Observable.of(this.userInfo);
        }
        try {
            const self = this;
            return this.authService.preloadProfileData().map(resp => {
                console.log(resp);
                self.userInfo = new UserInfo();
                self.userInfo.id = resp['customerId'] || resp['employeeId'] || resp['managerId'];
                self.userInfo.name = resp['customerName'];
                self.userInfo.address = resp['deliveryAddress'];
                return self.userInfo;
            });
        } catch (e) {
            console.log('No user is logged in');
        }
    }
}
