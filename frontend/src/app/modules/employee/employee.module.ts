import { NgModule } from '@angular/core';

import { OrdersComponent } from './orders.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
    declarations: [
      OrdersComponent
    ],
    exports: [
      OrdersComponent
    ]
})
export class EmployeeModule {

}
