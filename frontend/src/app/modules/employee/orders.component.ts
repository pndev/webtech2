import { Component, OnInit } from '@angular/core';
import { HttpClient } from '../../http-client.service';
import { Response } from '@angular/http';

@Component({
    templateUrl: './orders.component.html'
})
export class OrdersComponent implements OnInit {

  constructor(private http: HttpClient) {}

  private openOrders = [];
  private deliveredOrders = [];

  public ngOnInit() {
    this.refresh();
  }

  public refresh() {
    this.getOpenOrders();
    this.getDeliveredOrders();
  }

  public getOpenOrders() {
    const self = this;
    this.http.get('/api/employee/orders')
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .subscribe(orders => self.openOrders = orders);
  }

  public getDeliveredOrders() {
    const self = this;
    this.http.get('/api/employee/orders?delivered=true')
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .subscribe(orders => self.deliveredOrders = orders);
  }

  public markOrderDelivered(orderId) {
    this.http.post('/api/employee/orders/' + orderId + '/complete', {})
      .subscribe(resp => {
        this.refresh();
    });
  }
}
