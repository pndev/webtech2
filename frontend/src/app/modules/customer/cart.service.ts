import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

export class CartItem {
  constructor(public item: any, public count: number) {}
}

@Injectable()
export class CartService {

  private cart: CartItem[] = [];

  public addToCart(orderItem) {
    const found = this.cart.find(cartItem => cartItem.item._id === orderItem._id);
    if (found) {
      found.count += 1;
    } else {
      this.cart.push( {
        item: orderItem,
        count: 1
      });
    }
  }

  public removeFromCart(orderItem) {
    this.cart = this.cart.map(cartItem => cartItem.item._id === orderItem._id ? {
      item: cartItem.item,
      count: cartItem.count - 1
    } : {
      item: cartItem.item,
      count: cartItem.count
    }).filter (cartItem => cartItem.count > 0);
  }

  public removeAllFromCart(orderItem) {
    this.cart = this.cart.filter(cartItem => cartItem.item._id !== orderItem._id);
  }

  public clear() {
    this.cart = [];
  }

  public getItems(): CartItem[]  {
    return this.cart;
  }
}
