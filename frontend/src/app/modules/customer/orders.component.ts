import { Component, OnInit } from '@angular/core';
import { HttpClient } from '../../http-client.service';
import { Response } from '@angular/http';

@Component({
    templateUrl: './orders.component.html'
})
export class OrdersComponent implements OnInit {

  constructor(private http: HttpClient) {}

  private orders;

  public ngOnInit() {
    this.getOrders();
  }

  public getOrders() {
    const self = this;
    this.http.get('/api/customer/orders')
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .subscribe(orders => self.orders = orders);
  }
}
