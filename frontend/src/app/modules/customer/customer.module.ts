import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { ItemviewComponent } from './itemview.component';
import { OrdersComponent } from './orders.component';
import { CartService } from './cart.service';
import { CheckoutComponent } from './checkout.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule
    ],
    declarations: [
      ShopComponent,
      ItemviewComponent,
      OrdersComponent,
      CheckoutComponent
    ],
    exports: [
      ShopComponent,
      OrdersComponent,
      CheckoutComponent
    ],
  providers: [
    CartService
  ]
})
export class CustomerModule { }
