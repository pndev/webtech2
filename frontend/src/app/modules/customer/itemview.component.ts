import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-itemview',
    templateUrl: './itemview.component.html'
})
export class ItemviewComponent {
  @Output() onAddedToCart: EventEmitter<any> = new EventEmitter();
  @Input() item;

  public addToCart(item) {
    this.onAddedToCart.emit(item);
  }
}
