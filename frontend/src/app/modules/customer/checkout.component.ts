import { Component, OnInit } from '@angular/core';
import { HttpClient } from '../../http-client.service';
import { Response } from '@angular/http';
import { CartService } from './cart.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: './checkout.component.html'
})
export class CheckoutComponent implements OnInit {

  constructor(private http: HttpClient, private cart: CartService, private router: Router) {}

  private cartItems;
  private order;

  public ngOnInit() {
    this.cartItems = this.cart.getItems();
    if (this.cartItems.length === 0) {
      this.router.navigate(['/customer/shop']);
    }
  }

  public checkOut() {
    if (this.order) {
      return;
    }
     const self = this;
     this.http.post('/api/customer/orders', {
      orderItems: self.cartItems.map(coi => ({ item: coi.item._id, count: coi.count }) )
     })
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
     .subscribe(order => {
       self.order = order;
       self.cart.clear();
     });
  }

  public getTotalPrice() {
    return this.cartItems.map(coi => coi.item.price * coi.count).reduce((a, b) => a + b, 0);
  }
}
