import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth/auth.service';
import { HttpClient } from '../../http-client.service';
import { Response } from '@angular/http';
import { CartService } from './cart.service';
import { Router } from '@angular/router';

class OrderItem {
  constructor(_id: string,
      name: String,
      price: Number,
      ingredients: String[],
      itemType: String) {}
}

@Component({
    templateUrl: './shop.component.html'
})
export class ShopComponent implements OnInit {

  private foods;
  private drinks;
  private orders;

  constructor(private router: Router, private http: HttpClient, private cart: CartService) {
      this.foods = [];
      this.drinks = [];
      this.orders = [];
  }

  public ngOnInit() {
    this.getFoods();
    this.getDrinks();
  }

  public getFoods() {
      const self = this;
      this.http.get('/api/customer/foods')
        .map((res: Response) => {
            console.log(res.json());
            return res.json();
        })
        .subscribe(foods => self.foods = foods);
  }

  public getDrinks() {
      const self = this;
      this.http.get('/api/customer/drinks')
        .map((res: Response) => {
          console.log(res.json());
          return res.json();
        })
        .subscribe(drinks => self.drinks = drinks);
  }

  public addToCart(orderItem) {
    this.cart.addToCart(orderItem);
  }

  public removeFromCart(orderItem) {
    this.cart.removeFromCart(orderItem);
  }

  public removeAllFromCart(orderItem) {
    this.cart.removeAllFromCart(orderItem);
  }

  public cartItems() {
    return this.cart.getItems();
  }

    public checkOut() {
      this.router.navigate(['/customer/checkout']);
    }
}
