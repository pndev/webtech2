import {Component, OnInit} from '@angular/core';

import { AuthService } from '../auth/auth.service';
import { HttpClient } from '../../http-client.service';
import { Response } from '@angular/http';

@Component({
    templateUrl: './manager.component.html'
})
export class ManagerComponent implements OnInit {

  private orderedCount = [];
  private orderedtotalIncome = [];
  private orderedLabels = [];
  private stats = [];

    constructor(private auth: AuthService, private http: HttpClient) {

    }

    public ngOnInit() {
      this.getStats();
    }

    public getStats() {
      const self = this;
      this.http.get('/api/manager/orders/stats')
        .map((res: Response) => {
          console.log(res.json());
          return res.json();
        })
        .subscribe(stats => {
          self.orderedLabels = stats.map(s => s.name);
          self.orderedCount = stats.map(s => s.count);
          self.orderedtotalIncome = stats.map(s => s.fullIncome);
          self.stats = stats;
          console.log(self.orderedtotalIncome);
        });
    }
}
