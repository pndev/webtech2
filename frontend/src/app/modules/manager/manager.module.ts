import { NgModule } from '@angular/core';

import { ManagerComponent } from './manager.component';
import { ChartsModule } from 'ng2-charts';
import { CommonModule } from '@angular/common';
import {OrdersComponent} from "./orders.component";

@NgModule({
  imports: [
    ChartsModule,
    CommonModule
  ],
  declarations: [
      ManagerComponent,
      OrdersComponent
    ],
    exports: [
      ManagerComponent,
      OrdersComponent
    ]
})
export class ManagerModule { }
