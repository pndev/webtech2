import { Injectable, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { HttpClient } from '../../http-client.service';
import { Response } from '@angular/http';

@Injectable()
export class AuthService implements CanActivate  {

    private isLoggedInSubject = new BehaviorSubject<boolean>(this.isAuthenticated());
    private jwtHelper = new JwtHelperService();

    public hasRole(role: string): boolean {
      const accessToken = localStorage.getItem('token');
      if (!accessToken) {
        return false;
      }
      const tokenPayload = this.jwtHelper.decodeToken(accessToken);
      return role === tokenPayload.type;
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      const accessToken = localStorage.getItem('token');
      if (!accessToken) {
        return state.url.startsWith('/login');
      }
      const tokenPayload = this.jwtHelper.decodeToken(accessToken);

      console.log('Token: ' + JSON.stringify(tokenPayload));
      console.log('State: ' + state.url);

      if (tokenPayload.type === 'c') {
        return state.url.startsWith('/customer');
      } else if (tokenPayload.type === 'e') {
        return state.url.startsWith('/employee');
      } else if (tokenPayload.type === 'm') {
        return state.url.startsWith('/manager');
      } else {
        return false;
      }
    }

    constructor(private http: HttpClient, private router: Router) {}

    public getLoggedInEventStream(): Observable<boolean> {
        return this.isLoggedInSubject.asObservable().share();
    }

    public logout() {
      localStorage.removeItem('token');
      this.router.navigate(['/']);
    }

  public loginCustomer(loginId, name, address): void {
    this.http.post('/api/customer/login', {
      loginId: loginId,
      name: name,
      address: address
    }).map((res: Response) => {
      console.log(res.json());
      return res.json();
    })
      .subscribe(resp => {
        this.handleToken(resp.token);
        this.router.navigate(['/customer/shop']);
      });
  }

  public loginManager(loginId): void {
    this.http.post('/api/manager/login', {
      loginId: loginId
    }).map((res: Response) => {
      console.log(res.json());
      return res.json();
    })
      .subscribe(resp => {
        this.handleToken(resp.token);
        this.router.navigate(['/manager/stats']);
      });
  }

  public loginEmployee(loginId): void {
    this.http.post('/api/employee/login', {
      loginId: loginId
    }).map((res: Response) => {
      console.log(res.json());
      return res.json();
    })
      .subscribe(resp => {
        this.handleToken(resp.token);
        this.router.navigate(['/employee/orders']);
      });
  }

    public handleToken(token): void {
      localStorage.setItem('token', token);
    }

    public isAuthenticated(): boolean {
        return !!localStorage.getItem('token');
    }

    public preloadProfileData(): Observable<any> {
        const accessToken = localStorage.getItem('token');
        if (!accessToken) {
            return Observable.of(null);
        }
        const tokenPayload = this.jwtHelper.decodeToken(accessToken);
        const self = this;
        return Observable.create(observer => {
            observer.nex(tokenPayload);
            observer.onComplete();
          }
        );
    }
}
