import { OnInit, Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  templateUrl: './auth.component.html'
})
export class AuthComponent {

  public loginType = 'c';
  public loginId;
  public loginName;
  public address;

  constructor(private auth: AuthService) {

  }

  public login() {
    switch (this.loginType) {
      case 'c': this.auth.loginCustomer(this.loginId, this.loginName, this.address); break;
      case 'e': this.auth.loginEmployee(this.loginId); break;
      case 'm': this.auth.loginManager(this.loginId.address); break;
    }
  }
}
