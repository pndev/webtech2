import { NgModule } from '@angular/core';

import { AuthService } from './auth.service';
import { AuthComponent } from './auth.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
    declarations: [
      AuthComponent
    ],
    exports: [
      AuthComponent
    ],
    providers: [
        AuthService
    ]
})
export class AuthModule { }
