import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class HttpClient {

    constructor(private http: Http) { }

    createAuthorizationHeader(): Headers {
        const headers = new Headers();
        const token = localStorage.getItem('token');
        if (token) {
            headers.append('Authorization', 'Bearer ' + token);
        }
        return headers;
    }

    public get(url) {
        return this.http.get(url, {
            headers: this.createAuthorizationHeader()
        });
    }

    public post(url, data) {
        return this.http.post(url, data, {
            headers: this.createAuthorizationHeader()
        });
    }

    public put(url, data) {
        return this.http.put(url, data, {
            headers: this.createAuthorizationHeader()
        });
    }

    public delete(url) {
        return this.http.delete(url, {
            headers: this.createAuthorizationHeader()
        });
    }
}
