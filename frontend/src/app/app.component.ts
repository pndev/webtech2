import { OnInit, Component } from '@angular/core';
import { AuthService } from './modules/auth/auth.service'
import { UserInfoService, UserInfo } from './modules/userinfo/user-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements OnInit {

  private userInfo: UserInfo;

  constructor(private auth: AuthService, private userInfoService: UserInfoService) { }

  public hasRole(role: string): boolean {
    return this.auth.hasRole(role);
  }

  ngOnInit() {
    const self = this;
    this.auth.getLoggedInEventStream().subscribe(loggedIn => {
      if (!loggedIn) {
        self.userInfo = null;
        return;
      }
      this.userInfoService.getSimpleUserInfo().subscribe(info => {
        console.log('User logged in: ' + JSON.stringify(info));
        self.userInfo = info;
      });
    });
  }
}
