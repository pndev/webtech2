import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import { AuthModule } from './modules/auth/auth.module';
import { AuthComponent } from './modules/auth/auth.component';

import { CustomerModule } from './modules/customer/customer.module';
import { ShopComponent } from './modules/customer/shop.component';
import { EmployeeModule } from './modules/employee/employee.module';
import { ManagerModule } from './modules/manager/manager.module';
import { ManagerComponent } from './modules/manager/manager.component';

import { UserInfoService } from './modules/userinfo/user-info.service';
import { HttpClient } from './http-client.service';
import { CheckoutComponent } from './modules/customer/checkout.component';
import { OrdersComponent } from './modules/customer/orders.component';
import { OrdersComponent as EmployeeOrdersComponent } from './modules/employee/orders.component';
import { OrdersComponent as ManagerOrdersComponent } from './modules/manager/orders.component';
import { AuthService } from './modules/auth/auth.service';

const appRoutes: Routes = [{
  path: 'login',
  component: AuthComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: 'customer/shop',
  component: ShopComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: 'customer/checkout',
  component: CheckoutComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: 'customer/orders',
  component: OrdersComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: 'employee/orders',
  component: EmployeeOrdersComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: 'manager/stats',
  component: ManagerComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: 'manager/orders',
  component: ManagerOrdersComponent,
  canActivate: [
    AuthService
  ]
}, {
  path: '**',
  redirectTo: '/login'
}];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpModule,
    AuthModule,
    CustomerModule,
    EmployeeModule,
    ManagerModule,
    BrowserModule,
    RouterModule.forRoot(
      appRoutes, { enableTracing: true }
    )
  ],
  providers: [HttpClient, UserInfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
