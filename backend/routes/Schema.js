var mongoose = require('mongoose');

var foodData = require('./testData.json');

var db = mongoose.createConnection('mongodb://localhost:27017/foodDelivery', {autoIndex : true});

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // we're connected!
    console.log('MongoDB is Open');
});

var Schema = mongoose.Schema;

var OrderItemType = {
    _id : Schema.ObjectId,
    name : String,
    price : Number,
    ingredients: [String],
    itemType : {
        type : String,
        enum  : [ "food", "drink" ]
    }
};

var OrderSchema = new Schema({
    _id : Schema.ObjectId,
    employeeId : String,
    customerId : String,
    deliveryAddress: String,
    orderItems : [{
        item: OrderItemType,
        count: Number
    }],
    totalCost : Number,
    status : {
        type : String,
        enum : [ "open", "delivered" ]
    }
});

var OrderItemSchema = new Schema(OrderItemType);

var EmployeeSchema = new Schema({
    _id : Schema.ObjectId,
    name : String
});

var CustomerSchema = new Schema({
    _id : Schema.ObjectId,
    name : String,
    billingAddress : String
});

module.exports.db = db;
module.exports.orders = db.model('orders', OrderSchema);
module.exports.orderItems = db.model('orderItems', OrderItemSchema);
module.exports.employees = db.model('employees', EmployeeSchema);
module.exports.customers = db.model('customers', CustomerSchema);

var OrderItem = db.model('orderItems');

for( var i = 0; i < foodData.length; i++ ) {
	foodData[i]._id =  new mongoose.Types.ObjectId();
	new OrderItem( foodData[ i ] ).save();
}
