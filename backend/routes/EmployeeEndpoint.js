var express = require('express');
var router = express.Router();
var dao = require('./Schema');
var mongoose = require('mongoose');
var auth = require('../services/authService');

router.get('/employee/orders',function(req,res) {
    var employee = auth.getPrincipal(req).employeeId;
    if (req.query.delivered) {
        dao.orders.find({ employeeId: employee, status: "delivered" }).exec(function(err, doc) {
            res.status(200).json(doc.slice().reverse());
        });
    } else {
        dao.orders.find({ status: "open" }).exec(function(err, doc) {
            res.status(200).json(doc.slice().reverse());
        });
    }
});

router.post('/employee/orders/:orderId/complete',function(req,res) {

    var employee = auth.getPrincipal(req).employeeId;
    var orderId = req.params.orderId;

    dao.orders.update( { _id: orderId }, { $set: { employeeId: employee, status: "delivered" }}, function (err, doc) {
        if(err){
            res.status(500).json({ error: "Application error" });
            return console.log(err);
        }
        res.status(200).json(doc);
    });
});

module.exports = router;