var express = require('express');
var router = express.Router();
var dao = require('./Schema');
var mongoose = require('mongoose');
var auth = require('../services/authService');

router.get('/customer/foods',function(req,res) {
    dao.orderItems.find({ itemType: 'food' }).exec(function(err, doc) {
        res.status(200).json(doc);
    });
});

router.get('/customer/drinks',function(req,res) {
    dao.orderItems.find({ itemType: 'drink' }).exec(function(err, doc) {
        res.status(200).json(doc);
    });
});

router.get('/customer/orders',function(req,res){
    var customer = auth.getPrincipal(req).customerId;
    dao.orders.find({ customerId: customer }).exec(function(err, doc) {
        res.status(200).json(doc.slice().reverse());
    });
});

router.post('/customer/orders',function(req,res) {

    var principal = auth.getPrincipal(req);
    var customer = principal.customerId;
    var deliveryAddress = principal.deliveryAddress;

    var cartItems = req.body['orderItems'];

    if (cartItems.length == 0) {
        return res.status(400).json({ error: "Error: invalid number of order items" });
    }

    dao.orderItems.find({}).exec(function(err,doc) {
        if (err) {
            return res.status(400).json({ error: "Error: " + err });
        }
        if (doc.length == 0) {
            return res.status(400).json({ error: "Error: invalid number of order items" });
        }

        const orderItemsMap = {};
        for (var i in doc) {
            const oi = doc[i];
            orderItemsMap[oi._id] = oi;
        }

        const itemsInCart = cartItems.map(function(coi) { return {
            item: orderItemsMap[coi.item],
            count: coi.count
        }; });

        const fullPrice = cartItems
            .map(function(coi) { return orderItemsMap[coi.item].price * coi.count; })
            .reduce(function(p1, p2) { return p1 + p2; });

        dao.orders.create({
            _id: new mongoose.Types.ObjectId(),
            customerId : customer,
            deliveryAddress : deliveryAddress,
            orderItems : itemsInCart,
            totalCost : fullPrice,
            status : 'open'
        }, function (err, doc) {
            if(err){
                return res.status(400).json({ error: "Error: " + err });
            }
            res.status(200).json(doc);
        });
    });
});

module.exports = router;