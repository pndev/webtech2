var express = require('express');
var router = express.Router();
var dao = require('./Schema');
var mongoose = require('mongoose');
var auth = require('../services/authService');

router.post('/customer/login',function(req,res) {
    res.status(200).json({token: auth.createUserToken(req.body.loginId, req.body.name, req.body.address)});
});

router.post('/employee/login',function(req,res) {
    res.status(200).json({token: auth.createEmployeeToken(req.body.loginId, false)});
});

router.post('/manager/login',function(req,res) {
    res.status(200).json({token: auth.createEmployeeToken(req.body.loginId, true)});
});

module.exports = router;