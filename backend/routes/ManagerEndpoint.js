var express = require('express');
var router = express.Router();
var dao = require('./Schema');
var mongoose = require('mongoose');

Array.prototype.flatMap = function(lambda) {
    return [].concat.apply([], this.map(lambda));
};

router.get('/manager/orderItems',function(req,res){
    if (!req.body) return res.sendStatus(400);
    dao.orderItems.create({
        _id: new mongoose.Types.ObjectId(),
        name : req.body['name'],
        price : req.body['price'],
        ingredients: req.body['ingredients'],
        itemType : req.body['itemType']
    }, function (err, doc) {
        if(err){
            res.status(500).json({ error: "Application error" });
            return console.log(err);
        }
        res.status(200).json(doc);
    });
});

router.post('/manager/orderItems',function(req,res){
    if (!req.body) return res.sendStatus(400);
    dao.orderItems.create({
        _id: new mongoose.Types.ObjectId(),
        name : req.body['name'],
        price : req.body['price'],
        ingredients: req.body['ingredients'],
        itemType : req.body['itemType']
    }, function (err, doc) {
        if(err){
            res.status(500).json({ error: "Application error" });
            return console.log(err);
        }
        res.status(200).json(doc);
    });
});

router.get('/manager/orders',function(req,res){
    dao.orders.find({}).exec(function(err, doc) {
        res.status(200).json(doc.slice().reverse());
    });
});

router.get('/manager/orders/stats',function(req,res){
    dao.orders.find({}).exec(function(err, doc) {
        const itemNameToCount = doc.flatMap(function(order) { return order.orderItems;  })
            .reduce(function (groups, orderItem) {
                const itemName = orderItem.item.name;
                groups[itemName] = (groups[itemName] || { count: 0, totalIncome: 0 })
                groups[itemName].count += orderItem.count;
                groups[itemName].totalIncome += orderItem.count * orderItem.item.price;
                return groups;
            }, Object.create(null));
        const stats = [];
        for (var name in itemNameToCount) {
            stats.push({
                name: name,
                count: itemNameToCount[name].count,
                fullIncome: itemNameToCount[name].totalIncome
            })
        }
        res.status(200).json(stats);
    });
});

router.get('/manager/purgeall', function(req,res){
    dao.db.dropDatabase(function(err, result){
        console.log('Error: ' + err + '; result: ' + result);
    });
    console.log('Database purged');
});

module.exports = router;