var jwt = require('jsonwebtoken');

const secretKey = 'thisIsAVerySecretKey';



function getPrincipal(req) {
    return req.user;
}

function createUserToken(username, fullName, address) {
    return jwt.sign({
        customerId: username,
        customerName: fullName,
        deliveryAddress: address,
        type: 'c'
    }, secretKey);
}

function createEmployeeToken(employeeId, isManager) {
    return jwt.sign({
        employeeId: employeeId,
        type: isManager ? 'm' : 'e'
    }, secretKey);
}

module.exports = {
    secret: secretKey,
    getPrincipal: getPrincipal,
    createUserToken: createUserToken,
    createEmployeeToken: createEmployeeToken
}