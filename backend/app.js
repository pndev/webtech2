/**
 * Created by tothzs on 2018.03.05..
 */
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');
var auth = require('./services/authService')

var app = express();

var authApi = require('./routes/AuthEndpoint');
var customerApi = require('./routes/CustomerEndpoint');
var managerApi = require('./routes/ManagerEndpoint');
var employeeApi = require('./routes/EmployeeEndpoint');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));

app.use('/api', authApi);
app.use('/api', jwt({ secret: auth.secret }), customerApi);
app.use('/api', jwt({ secret: auth.secret }), managerApi);
app.use('/api', jwt({ secret: auth.secret }), employeeApi);

app.listen(8080, function(){console.log("Server listens on 8080.")});